def write_colloscope_groupe(colloscope, ngroupe, parametres):
    """
    Edite le colloscope pour le groupe ngroupe+1 au format .csv
    """
    file=open(parametres.nom_colloscope+'_groupe_'+str(ngroupe+1)+'.csv','w')
    colloscope_groupe=[semaine[ngroupe] for semaine in colloscope]
    line1=''
    line2=''
    for s in range(len(colloscope_groupe)):
        line1+='S'+str(s+1)+';'
        for creneau in colloscope_groupe[s]:
            line2+=creneau+' '
        line2+=';'
    line1+='\n'
    line2+='\n'
    write=break_lines_csv(line1+line2,10,2)
    file.write(write)
    file.close()


def find_creneau(semaine,creneau):
    """
    Renvoie le numero du groupe dans lequel se trouve le creneau pour la semaine
    """
    g=0
    while creneau not in semaine[g]:
        g+=1
        if g==len(semaine):
            break
    return g


def break_lines_csv(chaine,nbc,saut):
    """
    A partir d'une chaine de caractere au format csv avec separateur point virgule,
    renvoie une nouvelle chaine pour laquelle il n'y a que nbc colonnes et avec saut lignes vides entre
    chaque cesure.
    """
    Lines=[]
    temp=''
    compt=0
    state=0
    l=0
    for char in chaine:
        if state==0:
            temp+=char
            if char==';':
                compt+=1
                if compt==nbc:
                    Lines.append(temp+'\n')
                    temp=''
                    compt=0
            if char=='\n':
                Lines.append(temp)
                temp=''
                compt=0
                state=1
        else:
            temp+=char
            if char==';':
                compt+=1
                if compt==nbc:
                    Lines[l]+=temp+'\n'
                    temp=''
                    compt=0
                    l+=1
            if char=='\n':
                Lines[l]+=temp
                temp=''
                compt=0
                l=0
    split=''
    emptyline=' ;'*nbc+'\n'
    for line in Lines:
        split+=line+emptyline*saut
    return split


def write_partial_colloscope_colleur(colloscope,colleur,startweek,lastweek,parametres, legende, colleurs):
    """
    Renvoie une chaine de caractere au format .csv avec separateur ;
    qui donne le colloscope pour le colleur de la semaine startweek a la semaine lastweek
    """
    creneaux=colleurs[colleur]['creneaux']
    line1=' ;'
    for s in range(startweek,lastweek):
        line1+='S'+str(s+1)+';'
    line1+='\n'
    lines=[legende[creneau]['jour']+' '+convert_time(parametres.nbslots,legende[creneau]['debut'])+';' for creneau in creneaux]
    C=[colloscope[s] for s in range(startweek,lastweek)]
    for semaine in C:
        c=0
        for creneau in creneaux:
            g=find_creneau(semaine,creneau)
            if g==len(semaine):
                lines[c]+=';'
            else:
                lines[c]+='G'+str(g+1)+';'
            c+=1
    for l in range(len(lines)):
        lines[l]+='\n'
    write=line1
    for line in lines:
        write+=line
    return write


def write_colloscope_colleur(colloscope,colleur,parametres, legende, colleurs):
    """
    Edite le colloscope pour le colleur specifie au format .csv avec separateur ;
    avec un nombre de colonnes max egal a nbc+1 et des sauts de ligne donnes par la variable entiere "saut"
    """
    nbc,saut = parametres.colonnes_colloscope, parametres.saut_de_ligne
    file=open(parametres.nom_colloscope+'_'+colleur+'.csv','w')
    write=''
    nbsemaines=len(colloscope)
    emptyline=' ;'*nbc+'\n'
    r=nbsemaines//nbc
    if nbsemaines%nbc!=0:
        r+=1
    for j in range(r):
        write+=write_partial_colloscope_colleur(colloscope,colleur,(nbc)*j,min((nbc)*(j+1),nbsemaines), legende, colleurs)
        write+=emptyline*saut
    file.write(write)
    file.close()


def convert_time(nbslots,time):
    """
    Convertit un temps donne en "slots" en temps classique format 24h
    """
    nbminutes_par_slot=(24*60)//nbslots
    total_minutes=int(time)*nbminutes_par_slot
    heure=total_minutes//60
    minutes=total_minutes%60
    return str(heure)+'h'+str(minutes)


def get_sigle_number(creneau, legende, matieres):
    """
    Renvoie l'entier correspondant au creneau sans le sigle
    """
    sigle=matieres[legende[creneau]['matiere']]['sigle']
    return int(creneau[len(sigle):len(creneau)])


def merge_lists(L):
    """
    L est une liste de liste de chaines de caracteres triee par longueur (decroissant).
    Renvoie une nouvelle liste ou les listes les plus petites "fusionnent" pour atteindre le plus proche
    de la longueur de la plus grande - (le nombre de listes fusionnees -1)
    """
    if len(L)<=2:
        return L
    maxl=len(L[0])
    j=1
    while j<len(L)-1:
        l=len(L[j])
        i=j+1
        while l+len(L[i])+1>maxl:
            i+=1
            if i==len(L):
                break
        if i==len(L):
            j+=1
        else:
            L[j]+=['; ; ;']+L[i]
            del(L[i])
            L=sorted(L,key=len,reverse=True)
            return merge_lists(L)
    return L


def write_legende(parametres, legende, matieres):
    """
    Edite la legende du colloscope au format .csv
    Au maximum nbc*2 colonnes (cad nbc matieres)
    """

    nbc = parametres.colonnes_legende
    file=open(parametres.nom_colloscope+'_Legende.csv','w')
    Columns={}
    for matiere in matieres:
        if matieres[matiere]['Legende'] not in Columns:
            Columns[matieres[matiere]['Legende']]=sorted(matieres[matiere]['creneaux'],
                                                         key=(lambda creneau: get_sigle_number(creneau, legende, matieres)))
        else:
            Columns[matieres[matiere]['Legende']]+=matieres[matiere]['creneaux']
            Columns[matieres[matiere]['Legende']]=sorted(Columns[matieres[matiere]['Legende']],
                                                         key=(lambda creneau: get_sigle_number(creneau, legende, matieres)))
    for matiere in Columns:
        for c in range(len(Columns[matiere])):
            creneau=Columns[matiere][c]
            Columns[matiere][c]+=';'+legende[creneau]['colleur']+';'+legende[creneau]['jour']+' '+convert_time(parametres.nbslots,legende[creneau]['debut'])+';'
    Full_columns=sorted([[matiere+'; ; ;']+Columns[matiere] for matiere in Columns],key=len,reverse=True)
    c=0
    Final_columns=merge_lists(Full_columns)
    Final_columns=sorted(Final_columns,key=len,reverse=True)
    write=''
    j=0
    while j<len(Final_columns)//nbc+1:
        Temp=Final_columns[nbc*j:nbc*j+nbc]
        Lines=['']*len(Temp[0])
        for column in Temp:
            for l in range(len(Lines)):
                if l<len(column):
                    Lines[l]+=column[l]+';'
                else:
                    Lines[l]+='; ; ; ;'
        for l in range(len(Lines)):
            Lines[l]+='\n'
        for line in Lines:
            write+=line
        emptyline=';'*nbc*3+'\n'
        write+=emptyline*2
        j+=1
    file.write(write)
    file.close()


def write_full_colloscope(colloscope,parametres):
    """
    Edite le colloscope complet au format .csv
    """

    if colloscope == None:
        return "Pas de solutions trouvee"

    nbc,saut=parametres.colonnes_colloscope,parametres.saut_de_ligne
    write=''
    c=0
    for i in range(parametres.nbsemaines):
        if c%nbc==0:
            write+=';'
        write+='S'+str(i+1)+';'
        c+=1
    write+='\n'
    for j in range(parametres.nbgroupes):
        c=0
        for semaine in colloscope:
            if c%nbc==0:
                write+='G'+str(j+1)+';'
            for creneau in semaine[j]:
                write+=creneau+' '
            write+=';'
            c+=1
        write+='\n'
    return break_lines_csv(write,nbc+1,saut)


def write_everything(colloscope, parametres, legende, colleurs, imprimer):
    if imprimer:
        write_legende(parametres)
    colloscope_global_imprime = write_full_colloscope(colloscope,parametres)
    if imprimer:
        for colleur in colleurs:
            write_colloscope_colleur(colloscope,colleur,parametres, legende, colleurs)
        for groupe in range(parametres.nbgroupes):
            write_colloscope_groupe(colloscope,groupe,parametres)
    return colloscope_global_imprime