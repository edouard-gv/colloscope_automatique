﻿import xml.etree.ElementTree as ET

def structure(nomfichier,nbslots,nbgroupes,nbsemaines,colonnes_legende,colonnes_colloscope,saut_de_ligne):
    """
    Crée un fichier nomfichier.xml
    avec la structure requise
    root[0] : "Matières"
    root[1] : "Professeurs"
    root[2] : "TP/TD"
    """
    racine=ET.Element('racine',nbslots=nbslots,nbgroupes=nbgroupes,nbsemaines=nbsemaines,colonnes_legende=colonnes_legende,colonnes_colloscope=colonnes_colloscope,saut_de_ligne=saut_de_ligne)
    mat=ET.SubElement(racine,'Matieres')
    prof=ET.SubElement(racine,'Professeurs')
    gr=ET.SubElement(racine,'TP')
    arbre=ET.ElementTree(racine)
    arbre.write(nomfichier)

def ajouter_matiere(nomfichier,matiere,periode,sigle,nombredecolles,debutperiode,Legende):
    """
    Ajoute une matiere au fichier nomfichier.xml
    """
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    M=root[0]
    newmat=ET.SubElement(M,matiere,periode=periode,sigle=sigle,nombredecolles=nombredecolles,debutperiode=debutperiode,Legende=Legende)
    tree.write(nomfichier)

def ajouter_professeur(nomfichier,professeur,matiere,poids):
    """
    Ajoute un professeur au fichier nomfichier.xml
    """
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    P=root[1]
    newprof=ET.SubElement(P,professeur,matiere=matiere,poids=poids)
    tree.write(nomfichier)

def ajouter_horaire(nomfichier,professeur,matiere,semaines,debut,longueur,jour):
    """
    Ajoute un horaire au professeur en entrée
    """
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    P=root[1]
    for prof in P:
        if prof.tag==professeur and prof.attrib['matiere']==matiere:
            ET.SubElement(prof,'horaire',semaines=semaines,debut=debut,longueur=longueur,jour=jour)
            break
    tree.write(nomfichier)

def ajouter_TP(nomfichier,intitule,periode,sigle,debutperiode):
    """
    Ajoute un TP au fichier nomfichier.xml
    """
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    TP=root[2]
    ET.SubElement(TP,intitule,periode=periode,sigle=sigle,debutperiode=debutperiode)
    tree.write(nomfichier)

def ajouter_horaire_TP(nomfichier,TP,debut,longueur,jour,places,semaines):
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    T=root[2]
    for tp in T:
        if tp.tag==TP:
            ET.SubElement(tp,'horaire',semaines=semaines,debut=debut,longueur=longueur,jour=jour,places=places)
            break
    tree.write(nomfichier)

def supprimer_matiere(nomfichier,matiere):
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    M=root[0]
    m=0
    for mat in M:
        if mat.tag==matiere:
            del(root[0][m])
            break
        m+=1
    tree.write(nomfichier)

def supprimer_colleur(nomfichier,colleur):
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    P=root[1]
    p=0
    for prof in P:
        if prof.tag==colleur:
            del(root[1][p])
            break
        p+=1
    tree.write(nomfichier)

def supprimer_horaire_colleur(nomfichier,colleur,matiere,jour,debut):
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    P=root[1]
    p=0
    while P[p].tag!=colleur or P[p].attrib['matiere']!=matiere:
        p+=1
        if p==len(P):
            break
    if p!=len(P):
        h=0
        for horaire in P[p]:
            if horaire.attrib['jour']==jour and horaire.attrib['debut']==debut:
                del(P[p][h])
                break
            h+=1
    tree.write(nomfichier)

def supprimer_TP(nomfichier,TP):
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    T=root[2]
    t=0
    for tp in T:
        if tp.tag==TP:
            del(root[2][t])
            break
        t+=1
    tree.write(nomfichier)

def supprimer_horaire_TP(nomfichier,TP,jour,debut):
    tree=ET.parse(nomfichier)
    root=tree.getroot()
    T=root[2]
    t=0
    while T[t].tag!=TP:
        t+=1
        if t==len(T):
            break
    if t!=len(T):
        h=0
        for horaire in T[t]:
            if horaire.attrib['jour']==jour and horaire.attrib['debut']==debut:
                del(T[t][h])
                break
            h+=1
    tree.write(nomfichier)

