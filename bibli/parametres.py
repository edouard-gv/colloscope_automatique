from bibli.edition_xml import ET


class Parametres:
    def __init__(self, nom_colloscope):
        self.nomfichier = nom_colloscope + '.xml'
        tree = ET.parse(self.nomfichier)
        root = tree.getroot()
        self.nbslots = int(root.attrib['nbslots'])
        self.nbsemaines = int(root.attrib['nbsemaines'])
        self.nbgroupes = int(root.attrib['nbgroupes'])
        self.colonnes_legende = int(root.attrib['colonnes_legende'])
        self.colonnes_colloscope = int(root.attrib['colonnes_colloscope'])
        self.saut_de_ligne = int(root.attrib['saut_de_ligne'])

        self.arbre_matieres = root[0]
        self.arbre_colleurs = root[1]
        self.arbre_tp = root[2]

        self.nbseeds = 6
        self.branching_rate = 2
        self.width = 4
        self.nblevels = 10
        self.death_rate = 0

    def infos_racine(self):
        return self.nomfichier, self.nbslots, self.nbsemaines, self.nbgroupes, self.colonnes_legende, self.colonnes_colloscope, self.saut_de_ligne
