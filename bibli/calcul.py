import random as rd

from bibli.list_manipulation import intersection


def overlap(cle1,cle2,legende):
    """
    Verifie si les creneaux cle1 et cle2 sont incompatibles ou pas (chevauchement dans le temps)
    renvoie True si cle1 et cle2 sont incompatibles, False sinon
    """
    if legende[cle1]['jour']!=legende[cle2]['jour']:
        return False
    b1=int(legende[cle1]['debut'])<int(legende[cle2]['debut'])+int(legende[cle2]['longueur'])
    b2=int(legende[cle2]['debut'])<int(legende[cle1]['debut'])+int(legende[cle1]['longueur'])
    return b1 and b2


def affect_cle(cle,semaine,groupe,pool):
    """
    affecte la cle au groupe dans la semaine et la retire du pool disponible
    """
    semaine[groupe].append(cle)
    pool.remove(cle)


def desaffect_cle(cle,semaine,groupe,pool):
    """
    Retire la cle au groupe dans laet la rajoute au pool disponible
    """
    semaine[groupe].remove(cle)
    pool.append(cle)


def groupes_possibles(cle,semaine,profils_semaine,colloscope, legende, matieres):
    """
    cle est un creneau qui est une cle du dico Legende genere par la fonction creation_Legende
    semaine est la semaine courante : semaine[i] est la liste des creneaux affectes au groupe i+1 pour l'instant.
    colloscope est la liste des semaines passees
    Matieres est le dictionnaire des matieres
    profils est une liste ou profil[i] est la liste des matieres obligatoires a affecter au groupe i+1
    Renvoie une liste des groupes qui pourraient etre affectes au creneau "cle".
    La liste en sortie est du type [ [numero du groupe,[creneaux_a_deplacer] ]
    ou [creneaux_a_deplacer] est la liste des creneaux deja affectes (dans la liste semaine) qu'il faudrait deplacer
    pour pouvoir affecter le groupe au creneau
    """
    #Recuperation des infos utiles
    matiere=legende[cle]['matiere']
    groupes_profiles=[i for i in range(len(semaine)) if matiere in profils_semaine[i]]
    #Cas ou la cle est une matiere periodique ou un TP
    if groupes_profiles!=[]:
        Output=[[i,[]] for i in groupes_profiles]
    #Cas ou la cle est une matiere non periodique (donc n'apparait pas dans les profils)
    else:
        Output=[]
        for i in range(len(semaine)):
            compt=0
            for S in colloscope:
                for cle1 in S[i]:
                    if legende[cle1]['matiere']==matiere:
                        compt+=1
            if compt<int(matieres[matiere]['nombredecolles']):
                Output.append([i,[]])
    #Construction de la liste de sortie
    for k in range(len(Output)):
        groupe=Output[k][0]
        for cle1 in semaine[groupe]:
            if legende[cle]['matiere']==legende[cle1]['matiere']:
                Output[k][1].append(cle1)
            elif overlap(cle,cle1,legende):
                Output[k][1].append(cle1)
    return Output


def init_semaine(numero_semaine, legende, profils, attrib):
    """
    Initialise la semaine numero_semaine+1.
    Renvoie :
    1) une liste semaine de longueur nbgroupes ou semaine[i]=[]
    2) le pool de creneaux disponibles cette semaine
    3) les profils pour chaque groupe pour cette semaine
    Legende est le dico renvoye par la fonction creation_legende
    Profils est la liste renvoyee par la fonction creation_profils
    Attrib est la liste renvoyee par la fonction attribution_profils
    """
    #Creation de la liste semaine
    nbgroupes=len(attrib)
    semaine=[[] for i in range(nbgroupes)]
    #Creation du pool de creneaux disponibles
    pool=[]
    for cle in legende:
        if legende[cle]['semaines'][numero_semaine]=='1':
            #Cas d'un TP
            if 'places' in legende[cle]:
                pool+=[cle]*int(legende[cle]['places'])
            else:
                pool.append(cle)
    #Creation des profils pour la semaine
    profils_semaine=[]
    for groupe in range(nbgroupes):
        profils_semaine.append(profils[(attrib[groupe]+numero_semaine)%len(profils)])
    return semaine,pool,profils_semaine


def affectation_simple(cle,semaine,profils_semaine,colloscope,creneaux_modifies,creneaux_prioritaires,pool, legende, matieres):
    """
    Essaie d'affecter le creneau cle a la semaine courante.
    On ne doit pas modifier un creneau de la liste creneaux_modifies. Si c'est impossible, on fait un reset sur les listes creneaux_modifies et
    creneaux_prioritaires
    """
    #On initialise les fonctions random
    rd.seed()
    #On recupere la liste des groupes
    GP=groupes_possibles(cle,semaine,profils_semaine,colloscope, legende, matieres)
    #On modifie la liste en tenant compte de la liste creneaux_modifies
    GP=[stuff for stuff in GP if intersection(stuff[1],creneaux_modifies)==[]]
    #On reset si on ne peut rien affecter
    if GP==[]:
        return semaine,pool,[],[]
    #On cherche une affectation qui "bouge" le moins de creneaux
    m=min([len(stuff[1]) for stuff in GP])
    Temp=[stuff for stuff in GP if len(stuff[1])==m]
    choix=rd.choice(Temp)
    #On affecte
    groupe=choix[0]
    affect_cle(cle,semaine,groupe,pool)
    #Cas ou il n'y a rien a deplacer
    if m==0:
        #Si il n'y a plus de creneaux prioritaires a affecter, on peut reset la liste creneaux_modifies
        if creneaux_prioritaires==[]:
            return semaine,pool,[],[]
        #Sinon on laisse tel quel
        else:
            return semaine,pool,creneaux_modifies,creneaux_prioritaires
    #Cas ou l'on deplace des creneaux
    else:
        creneaux_modifies.append(cle)
        for cle1 in choix[1]:
            desaffect_cle(cle1,semaine,groupe,pool)
            creneaux_prioritaires.append(cle1)
        return semaine,pool,creneaux_modifies,creneaux_prioritaires


def affectation_opt(cle,semaine,profils_semaine,colloscope,creneaux_modifies,creneaux_prioritaires,pool, legende, matieres, colleurs, probas, ecarts):
    """
    Idem que affectation simple sauf que l'on essaie d'ajouter la cle au groupe qui provoquera la variation de
    score la plus favorable
    """
    #On initialise les fonctions random
    rd.seed()
    #On recupere la liste des groupes
    GP=groupes_possibles(cle,semaine,profils_semaine,colloscope, legende, matieres)
    #On modifie la liste en tenant compte de la liste creneaux_modifies
    GP=[stuff for stuff in GP if intersection(stuff[1],creneaux_modifies)==[]]
    #On reset si on ne peut rien affecter
    if GP==[]:
        return semaine,pool,[],[]
    #On cherche une affectation qui "bouge" le moins de creneaux
    m=min([len(stuff[1]) for stuff in GP])
    temp=[stuff for stuff in GP if len(stuff[1])==m]
    #Parmi ces affectations, on cherche celles qui provoquent la variation de score la plus favorable (si la cle n'est pas un TP)
    if 'colleur' in legende[cle] and matieres[legende[cle]['matiere']]['periode']!='False':
        variations=[]
        for stuff in temp:
            colloscope_groupe=[s[stuff[0]] for s in colloscope]
            score_actuel=score(stuff[0],legende[cle]['colleur'],colloscope_groupe, legende, matieres, colleurs, probas, ecarts)
            colloscope_groupe+=[[cle]]
            nouveau_score=score(stuff[0],legende[cle]['colleur'],colloscope_groupe, legende, matieres, colleurs, probas, ecarts)
            variations.append(nouveau_score-score_actuel)
        mv=min(variations)
        temp=[temp[i] for i in range(len(temp)) if variations[i]==mv]
    elif 'colleur' in legende[cle]  and matieres[legende[cle]['matiere']]['periode']=='False':
        variations=[]
        for stuff in temp:
            colloscope_groupe=[s[stuff[0]] for s in colloscope]
            score_actuel=score(stuff[0],legende[cle]['colleur'],colloscope_groupe, legende, matieres, colleurs, probas, ecarts)\
                         +score_matiere(stuff[0],legende[cle]['matiere'],colloscope_groupe, legende, probas, ecarts)
            colloscope_groupe+=[[cle]]
            nouveau_score=score(stuff[0],legende[cle]['colleur'],colloscope_groupe, legende, matieres, colleurs, probas, ecarts)\
                          +score_matiere(stuff[0],legende[cle]['matiere'],colloscope_groupe, legende, probas, ecarts)
            variations.append(nouveau_score-score_actuel)
        mv=min(variations)
        temp=[temp[i] for i in range(len(temp)) if variations[i]==mv]
    #On choisit puis on affecte
    choix=rd.choice(temp)
    groupe=choix[0]
    affect_cle(cle,semaine,groupe,pool)
    #Cas ou il n'y a rien a deplacer
    if m==0:
        #Si il n'y a plus de creneaux prioritaires a affecter, on peut reset la liste creneaux_modifies
        if creneaux_prioritaires==[]:
            return semaine,pool,[],[]
        #Sinon on laisse tel quel
        else:
            return semaine,pool,creneaux_modifies,creneaux_prioritaires
    #Cas ou l'on deplace des creneaux
    else:
        creneaux_modifies.append(cle)
        for cle1 in choix[1]:
            desaffect_cle(cle1,semaine,groupe,pool)
            creneaux_prioritaires.append(cle1)
        return semaine,pool,creneaux_modifies,creneaux_prioritaires


def ajout_semaine(colloscope, legende, matieres, profils, attrib):
    """
    Essaie d'ajouter une semaine au colloscope
    """
    #Initialisation random
    rd.seed()
    #Initialisation des modifs
    creneaux_modifies=[]
    creneaux_prioritaires=[]
    #Recuperation des infos utiles
    numero_semaine=len(colloscope)
    semaine,pool,profils_semaine=init_semaine(numero_semaine, legende, profils, attrib)
    #Boucle principale
    while True:
        #On choisit si possible un creneau a affecter en eliminant les creneaux de TP surnumeraires
        temp=[cle for cle in pool]
        for cle in pool:
            if 'places' in legende[cle]:
                compt=0
                for groupe in semaine:
                    mat_groupe=[legende[cle1]['matiere'] for cle1 in groupe]
                    if legende[cle]['matiere'] in mat_groupe:
                        compt+=1
                    else:
                        break
                if compt==len(semaine):
                    temp.remove(cle)
        if temp==[]:
            break
        else:
            cle=rd.choice(temp)
            semaine,pool,creneaux_modifies,creneaux_prioritaires=affectation_simple(cle,semaine,profils_semaine,colloscope,creneaux_modifies,creneaux_prioritaires,pool, legende, matieres)
    colloscope.append(semaine)


def ajout_semaine_opt(colloscope, legende, matieres, colleurs, probas, ecarts, profils, attrib):
    """
    Idem que ajout_semaine en utilisant l'affectation optimisee
    """
    #Initialisation random
    rd.seed()
    #Initialisation des modifs
    creneaux_modifies=[]
    creneaux_prioritaires=[]
    #Recuperation des infos utiles
    numero_semaine=len(colloscope)
    semaine,pool,profils_semaine=init_semaine(numero_semaine, legende, profils, attrib)
    #Boucle principale
    emergency=0
    limit=100*len(pool)
    while True:
        #On choisit si possible un creneau a affecter en eliminant les creneaux de TP surnumeraires et en choisissant en priorite les creneaux avec le plus de "poids"
        temp=[cle for cle in pool]
        for cle in pool:
            if 'places' in legende[cle]:
                compt=0
                for groupe in semaine:
                    mat_groupe=[legende[cle1]['matiere'] for cle1 in groupe]
                    if legende[cle]['matiere'] in mat_groupe:
                        compt+=1
                    else:
                        break
                if compt==len(semaine):
                    temp.remove(cle)
        if temp==[]:
            break
        else:
            temp1=[cle for cle in temp if 'colleur' in legende[cle]]
            if temp1!=[]:
                max_poids=max([int(colleurs[legende[cle]['colleur']]['poids']) for cle in temp1 if 'colleur' in legende[cle]])
                temp1=[cle for cle in temp1 if int(colleurs[legende[cle]['colleur']]['poids'])==max_poids]
                cle=rd.choice(temp1)
            else:
                cle=rd.choice(temp)
            semaine,pool,creneaux_modifies,creneaux_prioritaires=affectation_opt(cle,semaine,profils_semaine,colloscope,creneaux_modifies,creneaux_prioritaires,pool, legende, matieres, colleurs, probas, ecarts)
        emergency+=1
        if emergency>=limit:
            return 'Help'
    colloscope.append(semaine)


def score(groupe,colleur,colloscope_groupe, legende, matieres, colleurs, probas, ecarts):
    """
    colloscope_groupe est une liste de listes ou colloscope_groupe[i] est la liste des creneaux attribues au groupe pour la semaine i+1
    Renvoie le score groupe/colleur dans l'attribution des colles pour le groupe
    La liste Probas doit avoir ete definie
    """
    #Calcul du score global
    compt=0
    rencontres=[]
    for i in range(len(colloscope_groupe)):
        for cle in colloscope_groupe[i]:
            if 'colleur' in legende[cle]:
                if legende[cle]['colleur']==colleur:
                    compt+=1
                    rencontres.append(i)
    th=sum(probas[(groupe,colleur)][0:len(colloscope_groupe)])
    temp=abs(compt-th)
    if temp<1:
        score_global=0
    else:
        score_global=int((temp/th)*100)
    #Calcul du score d'ecart entre chaque rencontre. On ajoute "l'ecart" entre la derniere rencontre et la derniere semaine+1 qu'on ajoute au score si il est superieur a l'ecart theorique
    score_ecarts=0
    #Cas ou la matiere est periodique
    if matieres[colleurs[colleur]['matiere']]['periode']!='False':
        for k in range(len(rencontres)-1):
            th=ecarts[(groupe,colleur)][rencontres[k]]
            temp=abs((rencontres[k+1]-rencontres[k]-th)/int(matieres[colleurs[colleur]['matiere']]['periode']))
            if temp>=1:
             score_ecarts+=int((temp/th)*100*int(matieres[colleurs[colleur]['matiere']]['periode']))
        if rencontres!=[]:
            th=ecarts[(groupe,colleur)][rencontres[-1]]
            temp=(len(colloscope_groupe)-rencontres[-1]-th)/int(matieres[colleurs[colleur]['matiere']]['periode'])
            if temp>=1:
                score_ecarts+=int((temp/th)*100*int(matieres[colleurs[colleur]['matiere']]['periode']))
    #Cas ou la matiere n'est pas periodique
    else:
        for k in range(len(rencontres)-1):
            th=ecarts[(groupe,colleur)][rencontres[k]]
            temp=abs(rencontres[k+1]-rencontres[k]-th)
            if temp>=1:
                score_ecarts+=int((temp/th)*100)
        if rencontres!=[]:
            th=ecarts[(groupe,colleur)][rencontres[-1]]
            temp=(len(colloscope_groupe)-rencontres[-1]-th)
            if temp>=1:
                score_ecarts+=int((temp/th)*100)
    return (score_ecarts+score_global)*int(colleurs[colleur]['poids'])


def score_matiere(groupe,matiere,colloscope_groupe, legende, probas, ecarts):
    """
    Calcule le score pour une matiere non periodique (meme methode que pour un colleur)
    """
    #Calcul du score global
    compt=0
    rencontres=[]
    for i in range(len(colloscope_groupe)):
        for cle in colloscope_groupe[i]:
            if legende[cle]['matiere']==matiere:
                compt+=1
                rencontres.append(i)
    th=sum(probas[(groupe,matiere)][0:len(colloscope_groupe)])
    temp=abs(compt-th)
    if temp<1:
        score_global=0
    else:
        score_global=int((temp/th)*100)
    #Calcul du score d'ecart entre chaque rencontre. On ajoute "l'ecart" entre la derniere rencontre et la derniere semaine+1 qu'on ajoute au score si il est superieur a l'ecart theorique
    score_ecarts=0
    for k in range(len(rencontres)-1):
        th=ecarts[(groupe,matiere)][rencontres[k]]
        temp=abs(rencontres[k+1]-rencontres[k]-th)
        if temp>=1:
            score_ecarts+=int((temp/th)*100)
    if rencontres!=[]:
        th=ecarts[(groupe,matiere)][rencontres[-1]]
        temp=(len(colloscope_groupe)-rencontres[-1]-th)
        if temp>=1:
            score_ecarts+=int((temp/th)*100)
    return score_global+score_ecarts


def score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts):
    """
    Retourne le score du colloscope (somme des scores pour chaque groupe et chaque colleur)
    """
    s=0
    for couple in probas:
        if couple[1] not in matieres:
            groupe=couple[0]
            colleur=couple[1]
            colloscope_groupe=[semaine[groupe] for semaine in colloscope]
            s+=score(groupe,colleur,colloscope_groupe, legende, matieres, colleurs, probas, ecarts)
        else:
            groupe=couple[0]
            matiere=couple[1]
            colloscope_groupe=[semaine[groupe] for semaine in colloscope]
            s+=score_matiere(groupe,matiere,colloscope_groupe, legende, probas, ecarts)
    return s


def newtrunks(Forest,Scores,parametres, legende, matieres, colleurs, probas, ecarts, profils, attrib):
    """
    Fait pousser des troncs dans la foret
    """
    for i in range(parametres.nbseeds):
        colloscope=[]
        for semaine in range(parametres.nbsemaines//parametres.nblevels+parametres.nbsemaines%parametres.nblevels):
            Test=ajout_semaine_opt(colloscope, legende, matieres, colleurs, probas, ecarts, profils, attrib)
            if Test!=None:
                break
        if Test==None:
            s=score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts)
            refine(colloscope,1000,0,1, legende, matieres, colleurs, probas, ecarts, profils, attrib)
            s1=score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts)
            while s1<s:
                refine(colloscope,1000,0,1, legende, matieres, colleurs, probas, ecarts, profils, attrib)
                s=s1
                s1=score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts)
            if None in Scores[0]:
                index=Scores[0].index(None)
                Scores[0][index]=s
                Forest[0][index]=colloscope
            else:
                M=max(Scores[0])
                if s<=M:
                    index=Scores[0].index(M)
                    Scores[0][index]=s
                    Forest[0][index]=colloscope


def copie_colloscope(colloscope):
    """
    Renvoie une copie independante du colloscope en entree
    """
    copie=[]
    for semaine in colloscope:
        copie.append([])
        for groupe in semaine:
            copie[-1].append([])
            for creneau in groupe:
                copie[-1][-1].append(creneau)
    return copie


def newbranches(Forest,Scores,height,parametres, legende, matieres, colleurs, probas, ecarts, profils, attrib):
    """
    Fait pousser des branches a partir du niveau height
    """
    for w in range(parametres.width):
        for i in range(parametres.branching_rate):
            if Scores[height][w]!=None:
                colloscope=copie_colloscope(Forest[height][w])
                for j in range(parametres.nbsemaines//parametres.nblevels):
                    Test=ajout_semaine_opt(colloscope, legende, matieres, colleurs, probas, ecarts, profils, attrib)
                    if Test!=None:
                        break
                if Test==None:
                    s=score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts)
                    refine(colloscope,1000,0,1, legende, matieres, colleurs, probas, ecarts, profils, attrib)
                    s1=score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts)
                    while s1<s:
                        refine(colloscope,1000,0,1, legende, matieres, colleurs, probas, ecarts, profils, attrib)
                        s=s1
                        s1=score_colloscope(colloscope, legende, matieres, colleurs, probas, ecarts)
                    if None in Scores[height+1]:
                        index=Scores[height+1].index(None)
                        Scores[height+1][index]=s
                        Forest[height+1][index]=colloscope
                    else:
                        M=max(Scores[height+1])
                        if s<=M:
                            index=Scores[height+1].index(M)
                            Scores[height+1][index]=s
                            Forest[height+1][index]=colloscope


def nextgen(Forest,Scores,parametres, legende, matieres, colleurs, probas, ecarts, profils, attrib):
    """
    Une nouvelle generation pour la foret
    """

    retour_forest = None
    newtrunks(Forest,Scores,parametres, legende, matieres, colleurs, probas, ecarts, profils, attrib)
    for height in range(len(Forest)-1):
        newbranches(Forest,Scores,height,parametres, legende, matieres, colleurs, probas, ecarts, profils, attrib)
    if None not in Scores[-1]:
        index=Scores[-1].index(min(Scores[-1]))
        retour_forest = Forest[-1][index]
    rd.seed()
    for i in range(len(Forest)-1):
        for w in range(parametres.width):
            p=rd.random()
            if p<parametres.death_rate:
                Scores[i][w]=None
    return retour_forest


def initforest(parametres):
    """
    Initialise la foret
    """
    Forest=[]
    Scores=[]
    for height in range(parametres.nblevels):
        Temp=[[] for w in range(parametres.width)]
        Forest.append(Temp)
        Temp=[None for w in range(parametres.width)]
        Scores.append(Temp)
    return Forest,Scores


def echange_possible(cle1,cle2,ngroupe1,ngroupe2,nsemaine,colloscope, legende):
    """
    Verifie si un echange de groupes est possible entre cle1 et cle2. cle1 est affectee au groupe ngroupe1+1 et cle2 au groupe ngroupe2+1
    pour la semaine nsemaine+1 dans le colloscope.
    Renvoie True si c'est le cas, False sinon
    """
    if legende[cle1]['matiere']!=legende[cle2]['matiere']:
        return False
    check1=[cle for cle in colloscope[nsemaine][ngroupe1] if cle!=cle1 and overlap(cle,cle2, legende)]
    if check1!=[]:
        return False
    check2=[cle for cle in colloscope[nsemaine][ngroupe2] if cle!=cle2 and overlap(cle,cle1, legende)]
    if check2!=[]:
        return False
    return True


def variation_score_echange(cle1,cle2,ngroupe1,ngroupe2,nsemaine,colloscope, legende, matieres, colleurs, probas, ecarts):
    """
    Calcule la variation de score apres un echange entre cle1 et cle2 dans la semaine nsemaine+1 du colloscope
    """
    colleur1=legende[cle1]['colleur']
    colloscope_groupe1=[semaine[ngroupe1] for semaine in colloscope]
    colleur2=legende[cle2]['colleur']
    colloscope_groupe2=[semaine[ngroupe2] for semaine in colloscope]
    sc=score(ngroupe1,colleur1,colloscope_groupe1, legende, matieres, colleurs, probas, ecarts)\
       +score(ngroupe2,colleur2,colloscope_groupe2, legende, matieres, colleurs, probas, ecarts)\
       +score(ngroupe1,colleur2,colloscope_groupe1, legende, matieres, colleurs, probas, ecarts)\
       +score(ngroupe2,colleur1,colloscope_groupe2, legende, matieres, colleurs, probas, ecarts)
    ncolloscope_groupe1=[]
    ncolloscope_groupe2=[]
    for s in range(len(colloscope)):
        if s!=nsemaine:
            ncolloscope_groupe1.append(colloscope_groupe1[s])
            ncolloscope_groupe2.append(colloscope_groupe2[s])
        else:
            ncolloscope_groupe1.append([])
            ncolloscope_groupe2.append([])
            for cle in colloscope_groupe1[s]:
                if cle!=cle1:
                    ncolloscope_groupe1[-1].append(cle)
            for cle in colloscope_groupe2[s]:
                if cle!=cle2:
                    ncolloscope_groupe2[-1].append(cle)
            ncolloscope_groupe1[-1].append(cle2)
            ncolloscope_groupe2[-1].append(cle1)
    nsc=score(ngroupe1,colleur1,ncolloscope_groupe1, legende, matieres, colleurs, probas, ecarts)\
        +score(ngroupe2,colleur2,ncolloscope_groupe2, legende, matieres, colleurs, probas, ecarts)\
        +score(ngroupe1,colleur2,ncolloscope_groupe1, legende, matieres, colleurs, probas, ecarts)\
        +score(ngroupe2,colleur1,ncolloscope_groupe2, legende, matieres, colleurs, probas, ecarts)
    return nsc-sc


def variation_score_insertion(cle,ngroupe1,ngroupe2,nsemaine,colloscope, legende, matieres, colleurs, probas, ecarts):
    """
    Calcule la variation de score apres l'insertion de cle dans le groupe ngroupe2 dans la semaine nsemaine+1 du colloscope.
    Initialement, cle est affectee a ngroupe1.
    A priori, la matiere est non periodique.
    """
    colleur=legende[cle]['colleur']
    matiere=legende[cle]['matiere']
    colloscope_groupe1=[semaine[ngroupe1] for semaine in colloscope]
    colloscope_groupe2=[semaine[ngroupe2] for semaine in colloscope]
    s=score(ngroupe1,colleur,colloscope_groupe1, legende, matieres, colleurs, probas, ecarts)\
      +score(ngroupe2,colleur,colloscope_groupe2, legende, matieres, colleurs, probas, ecarts)\
      +score_matiere(ngroupe1,matiere,colloscope_groupe1, legende, probas, ecarts)\
      +score_matiere(ngroupe2,matiere,colloscope_groupe2, legende, probas, ecarts)
    ncolloscope_groupe1=[]
    ncolloscope_groupe2=[]
    for s in range(len(colloscope)):
        if s!=nsemaine:
            ncolloscope_groupe1.append(colloscope_groupe1[s])
            ncolloscope_groupe2.append(colloscope_groupe2[s])
        else:
            ncolloscope_groupe1.append([])
            ncolloscope_groupe2.append([])
            for cle1 in colloscope_groupe1[s]:
                if cle!=cle1:
                    ncolloscope_groupe1[-1].append(cle1)
            for cle2 in colloscope_groupe2[s]:
                if cle!=cle2:
                    ncolloscope_groupe2[-1].append(cle2)
            ncolloscope_groupe2[-1].append(cle)
    ns=score(ngroupe1,colleur,ncolloscope_groupe1, legende, matieres, colleurs, probas, ecarts)\
       +score(ngroupe2,colleur,ncolloscope_groupe2, legende, matieres, colleurs, probas, ecarts)\
       +score_matiere(ngroupe1,matiere,colloscope_groupe1, legende, probas, ecarts)\
       +score_matiere(ngroupe2,matiere,colloscope_groupe2, legende, probas, ecarts)
    return ns-s


def refine(colloscope,Tinit,Tlimite,d, legende, matieres, colleurs, probas, ecarts, profils, attrib):
    """
    Essaie d'ameliorer le colloscope par des echanges de creneaux
    """
    #stats
    #Stats=[score_colloscope(colloscope)]
    T=Tinit
    #init random
    rd.seed()
    #boucle principale
    while T>Tlimite:
        #choix d'une semaine
        s=rd.randint(0,len(colloscope)-1)
        semaine,pool,profils_semaine=init_semaine(s, legende, profils, attrib)
        semaine=colloscope[s]
        #choix d'un creneau pour cette semaine
        cle1=rd.choice(pool)
        for k in range(len(semaine)):
            if cle1 in semaine[k]:
                ngroupe1=k
                break
        #Cas ou le creneau est une matiere periodique
        if 'colleur' in legende[cle1] and matieres[legende[cle1]['matiere']]['periode']!='False':
            L=[i for i in range(len(semaine)) if i!=ngroupe1]
            rd.shuffle(L)
            for ngroupe2 in L:
                Temp=[cle2 for cle2 in semaine[ngroupe2] if echange_possible(cle1,cle2,ngroupe1,ngroupe2,s,colloscope, legende)]
                if Temp!=[]:
                    cle2=Temp[0]
                    v=variation_score_echange(cle1,cle2,ngroupe1,ngroupe2,s,colloscope, legende, matieres, colleurs, probas, ecarts)
                    p=rd.random()
                    if v<=0:
                        colloscope[s][ngroupe1].remove(cle1)
                        colloscope[s][ngroupe2].remove(cle2)
                        colloscope[s][ngroupe1].append(cle2)
                        colloscope[s][ngroupe2].append(cle1)
                    break
        #Cas ou le creneau est une matiere non periodique
        elif 'colleur' in legende[cle1] and matieres[legende[cle1]['matiere']]['periode']=='False':
            GP=groupes_possibles(cle1,semaine,profils_semaine,colloscope, legende, matieres)
            GP=[stuff[0] for stuff in GP if stuff[1]==[]]
            L=[i for i in range(len(semaine)) if i!=ngroupe1]
            rd.shuffle(L)
            for ngroupe2 in L:
                mat=[legende[cle2]['matiere'] for cle2 in semaine[ngroupe2]]
                if legende[cle1]['matiere'] in mat:
                    Temp=[cle2 for cle2 in semaine[ngroupe2] if echange_possible(cle1,cle2,ngroupe1,ngroupe2,s,colloscope,legende)]
                    if Temp!=[]:
                        cle2=Temp[0]
                        v=variation_score_echange(cle1,cle2,ngroupe1,ngroupe2,s,colloscope, legende, matieres, colleurs, probas, ecarts)
                        p=rd.random()
                        if v<=0:
                            colloscope[s][ngroupe1].remove(cle1)
                            colloscope[s][ngroupe2].remove(cle2)
                            colloscope[s][ngroupe1].append(cle2)
                            colloscope[s][ngroupe2].append(cle1)
                            break
                elif ngroupe2 in GP:
                    v=variation_score_insertion(cle1,ngroupe1,ngroupe2,s,colloscope, legende, matieres, colleurs, probas, ecarts)
                    if v<=0:
                        colloscope[s][ngroupe1].remove(cle1)
                        colloscope[s][ngroupe2].append(cle1)
                        break
        #Cas ou le creneau est un TP
        else:
            L=[i for i in range(len(semaine)) if i!=ngroupe1]
            rd.shuffle(L)
            for ngroupe2 in L:
                Temp=[cle2 for cle2 in semaine[ngroupe2] if echange_possible(cle1,cle2,ngroupe1,ngroupe2,s,colloscope,legende)]
                if Temp!=[]:
                    cle2=Temp[0]
                    colloscope[s][ngroupe1].remove(cle1)
                    colloscope[s][ngroupe2].remove(cle2)
                    colloscope[s][ngroupe1].append(cle2)
                    colloscope[s][ngroupe2].append(cle1)
                    break
        T-=d
    #return Stats