﻿
def pgcd(x, y):
   """This function implements the Euclidian algorithm
   to find G.C.D. of two numbers"""

   while(y):
       x, y = y, x % y

   return x


def ppcm(x, y):
   """This function takes two
   integers and returns the L.C.M."""

   lcm = (x*y)//pgcd(x,y)
   return lcm

def Ppcm(L):
    """
    Renvoie le ppcm de tous les éléments de la liste L
    """
    m=1
    for l in L:
        m=ppcm(m,l)
    return m