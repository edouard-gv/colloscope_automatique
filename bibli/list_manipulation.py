﻿def deprive(list1,list2):
    """
    Renvoie la list1 privée de la list2
    """
    return [l for l in list1 if l not in list2]

def diffsym(list1,list2):
    """
    Renvoie la différence symétrique des deux listes
    """
    return deprive(list1,list2)+deprive(list2,list1)

def inclusion(list1,list2):
    """
    Vérifie si l'une des deux listes est incluse dans l'autre
    """
    return deprive(list1,list2)==[] or deprive(list2,list1)==[]

def intersection(list1,list2):
    """
    Renvoie l'intersection des deux listes
    """
    return [l for l in list1 if l in list2]

def countelement(L,D):
    """
    L is a list
    D a dictionnary which already contains a number of occurences for some elements
    Edit the dictionnary D to add all elements from L.
    """
    for e in L:
        if e in D:
            D[e]+=1
        else:
            D[e]=1
    return D

def firstnonzerodigit(n):
    """
    n is an int
    Return the exponent+1 of the first non zero digit in n (decimal writing)
    """
    if n==0:
        return 1
    i=0
    while n%10**i==0:
        i+=1
    return i