from bibli.arithmetique import Ppcm


def creation_profils(parametres):
    """
    Cree les "profils" (appairage des matieres par semaines) des groupes tout au long du ppcm des periodes
    donnees dans le fichier.
    Renvoie une liste de listes, par exemple [[Maths,Physique],[Maths,Anglais,SI],...]]
    """
    #Creation d'une liste de listes type [[matiere,periode,debutperiode],...]]
    M=[]
    for matiere in parametres.arbre_matieres:
        if matiere.attrib['periode']!='False':
            M.append([matiere.tag,int(matiere.attrib['periode']),int(matiere.attrib['debutperiode'])])
    #Creation d'une liste similaire pour les TP
    T=[]
    for tp in parametres.arbre_tp:
        T.append([tp.tag,int(tp.attrib['periode']),int(tp.attrib['debutperiode'])])
    #Initialisation de la liste des profils
    L=[m[1] for m in M]+[tp[1] for tp in T]
    p=Ppcm(L)
    profils=[]
    for i in range(p):
        profils.append([])
    #Creation des profils
    for mat in M:
        for i in range(p//mat[1]):
            profils[i*mat[1]+mat[2]].append(mat[0])
    for tp in T:
        for i in range(p//tp[1]):
            profils[i*tp[1]+tp[2]].append(tp[0])
    return profils


def creation_legende(parametres):
    """
    Cree une legende pour le colloscope.
    La legende est un dictionnaire ou les entrees sont des abreviations construites a partir du sigle et du numero de l'horaire
    dans le fichier (par exemple M1, P2, etc) et les valeurs sont des dictionnaires du type {matiere : , colleur (pour les colles) : , places (pour les TP) :
    , semaines : , jour : ,debut : longueur : }
    """
    #Creation de la liste des sigles et d'un dictionnaire {matiere : sigle }
    Sigle_matiere={}
    Sigles=[]
    for mat in parametres.arbre_matieres:
        Sigle_matiere[mat.tag]=mat.attrib['sigle']
        if mat.attrib['sigle'] not in Sigles:
            Sigles.append(mat.attrib['sigle'])
    for tp in parametres.arbre_tp:
        Sigle_matiere[tp.tag]=tp.attrib['sigle']
        if tp.attrib['sigle'] not in Sigles:
            Sigles.append(tp.attrib['sigle'])
    #Creation du compteur de sigles
    nbsigles=[0 for i in range(len(Sigles))]
    #Creation de la legende
    legende={}
    for prof in parametres.arbre_colleurs:
        for horaire in prof:
            sigle=Sigle_matiere[prof.attrib['matiere']]
            cle=sigle+str(nbsigles[Sigles.index(sigle)]+1)
            nbsigles[Sigles.index(sigle)]+=1
            legende[cle]=horaire.attrib
            legende[cle]['matiere']=prof.attrib['matiere']
            legende[cle]['colleur']= prof.tag
    for tp in parametres.arbre_tp:
        for horaire in tp:
            sigle=Sigle_matiere[tp.tag]
            cle=sigle+str(nbsigles[Sigles.index(sigle)]+1)
            nbsigles[Sigles.index(sigle)]+=1
            legende[cle]=horaire.attrib
            legende[cle]['matiere']=tp.tag
    return legende


def creation_matieres(parametres, legende):
    """
    Cree un dictionnaire avec les infos des matieres telle que specifiees dans le .xml en y ajoutant la liste des colleurs de cette matiere et les creneaux de cette matiere
    """
    #Ajout des infos de base
    matieres={}
    for mat in parametres.arbre_matieres:
        matieres[mat.tag]=mat.attrib
        matieres[mat.tag]['colleurs']=[]
        matieres[mat.tag]['creneaux']=[]
    #Ajout des colleurs
    for colleur in parametres.arbre_colleurs:
        matieres[colleur.attrib['matiere']]['colleurs'].append(colleur.tag)
    #Ajout des creneaux
    for cle in legende:
        if legende[cle]['matiere'] in matieres:
            matieres[legende[cle]['matiere']]['creneaux'].append(cle)
    return matieres


def creation_colleurs(parametres, legende):
    """
    Cree un dictionnaire avec les infos des colleurs
    """
    #Creation du dictionnaire
    colleurs={}
    for colleur in parametres.arbre_colleurs:
        colleurs[colleur.tag]=colleur.attrib
        colleurs[colleur.tag]['creneaux']=[]
    for cle in legende:
        if 'colleur' in legende[cle]:
            colleurs[legende[cle]['colleur']]['creneaux'].append(cle)
    return colleurs


def creation_probas(parametres, matieres, profils, attrib):
    """
    Cree un dictionnaire ou les cles sont tous les couples (numero_groupe,colleur)
    et les valeurs une liste probas ou probas[i] est la probabilite pour le groupe
    numero_groupe+1 d'avoir le colleur en semaine i+1
    Les listes Profils et Attrib et Matieres doivent avoir ete crees precedemment
    On ajoute egalement les couples (numero_groupe,matiere) pour toutes les matieres non periodiques
    """
    #Recuperation des heures disponibles
    HD=heures_disponibles(parametres)
    #Creation des probas
    probas={}
    for colleur in parametres.arbre_colleurs:
        hdcolleur=[0]*parametres.nbsemaines
        for horaire in colleur:
            for i in range(len(horaire.attrib['semaines'])):
                if horaire.attrib['semaines'][i]=='1':
                    hdcolleur[i]+=1
        for groupe in range(len(attrib)):
            probas[(groupe,colleur.tag)]=[]
            for j in range(parametres.nbsemaines):
                if colleur.attrib['matiere'] in profils[(attrib[groupe]+j)%len(profils)]:
                    probas[(groupe,colleur.tag)].append(hdcolleur[j]/HD[j][colleur.attrib['matiere']])
                elif matieres[colleur.attrib['matiere']]['nombredecolles']!='False':
                    probas[(groupe,colleur.tag)].append(hdcolleur[j]/parametres.nbgroupes)
                else:
                    probas[(groupe,colleur.tag)].append(0)
    for matiere in matieres:
        if matieres[matiere]['periode']=='False':
            for groupe in range(len(attrib)):
                probas[(groupe,matiere)]=[]
                for j in range(parametres.nbsemaines):
                    probas[(groupe,matiere)].append(HD[j][matiere]/parametres.nbgroupes)
    return probas


def creation_ecarts(probas):
    """
    Cree le dictionnaire des ecarts theoriques pour chaque colleur, chaque groupe et chaque semaine.
    Le format est le suivant : {(groupe,colleur): [ ecart[i]] } ou ecart[i] est l'ecart theorique a partir de la ieme semaine.
    """
    ecarts={}
    for couple in probas:
        ecarts[couple]=[]
        for i in range(len(probas[couple])):
            ecarts[couple].append(esperance_ecart(couple[0],couple[1],i, probas))
    return ecarts


def heures_disponibles(parametres):
    """
    Renvoie une liste HD ou HD[i] est un dictionnaire dont les cles sont les noms des
    matieres et les valeurs le nombre de creneaux disponibles pour la matiere en semaine i+1.
    """
    #Recuperation de la liste des matieres
    matieres=[]
    for mat in parametres.arbre_matieres:
        matieres.append(mat.tag)
    #Recuperation de la liste des TP
    TP=[]
    for tp in parametres.arbre_tp:
        TP.append(tp.tag)
    #Initialisation de la liste HD
    nbs = parametres.nbsemaines
    HD=[]
    for i in range(nbs):
        HD.append({})
        for mat in matieres:
            HD[-1][mat]=0
        for tp in TP:
            HD[-1][tp]=0
    #Construction de la liste HD
    for prof in parametres.arbre_colleurs:
        for horaire in prof:
            i=0
            for c in horaire.attrib['semaines']:
                if c=='1':
                    HD[i][prof.attrib['matiere']]+=1
                i+=1
    for tp in parametres.arbre_tp:
        for horaire in tp:
            i=0
            for c in horaire.attrib['semaines']:
                if c=='1':
                    HD[i][tp.tag]+=int(horaire.attrib['places'])
                i+=1
    return HD


def esperance_ecart(groupe,colleur,numero_semaine, probas):
    """
    Calcule l'esperance de l'ecart entre deux colles du colleur pour le groupe
    a partir de la semaine numero_semaine.
    On calcule jusqu'au moment ou l'on ne modifie plus que la deuxieme decimale apres une boucle complete de longueur nbsemaines.
    Si on depasse la derniere semaine, on retourne a la proba de la premiere.
    """
    nbsemaines=len(probas[(groupe,colleur)])
    p=1
    E=0
    temp=1
    boucle=0
    while temp>10**(-2):
        temp=0
        for i in range(nbsemaines):
            temp+=(boucle*nbsemaines+i+1)*probas[(groupe,colleur)][(numero_semaine+i+1)%nbsemaines]*p
            p=(1-probas[(groupe,colleur)][(numero_semaine+i+1)%nbsemaines])*p
        E+=temp
        boucle+=1
    return round(E,2)


def attribution_profils(parametres):
    """
    Essaie d'attribuer a chaque groupe une rotation de profils.
    """
    #Recuperation des profils
    profils= creation_profils(parametres)
    #Creation de la liste Attrib, ou Attrib[i] est le profil attribue au groupe i+1 en semaine 1
    attrib=[i%len(profils) for i in range(parametres.nbgroupes)]
    return attrib,profils


def verif_heures(attrib, profils, parametres):
    """
    Verifie la concordance entre les dispos et les besoins.
    Renvoie None si tout va bien, renvoie la liste des heures manquantes ou surnumeraires sinon.
    """
    #Recuperation des heures disponibles
    HD= heures_disponibles(parametres)
    #Creation de la liste des matieres periodiques
    Matieres_periodiques=[]
    for mat in parametres.arbre_matieres:
        if mat.attrib['periode']!='False':
            Matieres_periodiques.append(mat.tag)
    #Creation d'une liste besoins qui doit etre identique a la liste HD amputee des matieres
    #non periodiques
    besoins=[]
    Problemes=[]
    for i in range(len(HD)):
        besoins.append({})
        for matiere in Matieres_periodiques:
            besoins[-1][matiere]=0
    for j in range(len(HD)):
        for k in range(len(attrib)):
            for matiere in profils[(attrib[k] + j) % len(profils)]:
                if matiere in Matieres_periodiques:
                    besoins[j][matiere]+=1
    for j in range(len(HD)):
        for matiere in besoins[j]:
            if besoins[j][matiere]!=HD[j][matiere]:
                Problemes.append((j,matiere,HD[j][matiere],besoins[j][matiere]))
    #Creation de la liste des matieres non perodiques sous la forme [ [matiere,nombre de colles] ,...]
    Matieres_non_periodiques=[]
    for mat in parametres.arbre_matieres:
        if mat.attrib['periode']=='False':
            Matieres_non_periodiques.append([mat.tag,int(mat.attrib['nombredecolles'])])
    #Verification de la concordance entre les besoins et les dispos pour les matieres non periodiques
    for matiere in Matieres_non_periodiques:
        dispo=sum([HD[j][matiere[0]] for j in range(len(HD))])
        if dispo!=matiere[1]*parametres.nbgroupes:
            Problemes.append((None, matiere[0],dispo,matiere[1]*parametres.nbgroupes))
    #Creation de la liste des TP
    TP=[]
    for tp in parametres.arbre_tp:
        TP.append(tp.tag)
    #Verification de la concordance entre les besoins et les places dispos pour les tp (il peut y avoir trop de places dispos sans erreur)
    besoins=[]
    for i in range(len(HD)):
        besoins.append({})
        for tp in TP:
            besoins[-1][tp]=0
    for j in range(len(HD)):
        for k in range(len(attrib)):
            for tp in profils[(attrib[k] + j) % len(profils)]:
                if tp in TP:
                    besoins[j][tp]+=1
    for j in range(len(HD)):
        for tp in besoins[j]:
            if besoins[j][tp]>HD[j][tp]:
                Problemes.append((j,tp,HD[j][tp],besoins[j][tp]))
    return Problemes