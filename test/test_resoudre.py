import os
from os.path import isfile, join
from Colloscope_auto import resoudre, print_probleme


def test_print_probleme_pasassez():
    assert print_probleme(2, "Math", 4, 6) == "2 slots manquants pour Math en sem. 2"


def test_print_probleme_trop():
    assert print_probleme(3, "Physique", 6, 4) == "2 slots en trop   pour Physique en sem. 3"


def test_print_probleme_singulier():
    assert print_probleme(2, "Math", 5, 6) == "1 slot  manquant  pour Math en sem. 2"


def test_print_probleme_non_periodique():
    assert print_probleme(None, "Math", 5, 6) == "1 slot  manquant  pour Math sur l'annee"


def test_dorian():
    print(resoudre("test/data/colloscopeDorianPT2018-19", 2, False))

def test_files():
    repetoire = "test/data"
    noms_fichiers_in = [join(repetoire, f) for f in os.listdir(repetoire) if isfile(join(repetoire, f)) and f.endswith(".xml")]

    for nom_fichier_in in noms_fichiers_in:
        nom_colloscope = nom_fichier_in[:-4]
        nom_fichier_out = nom_colloscope + '.csv'
        resulat, colloscope = resoudre(nom_colloscope, 1, False)

        try:
            fichier_out = open(nom_fichier_out, 'r')
            assert ignorer_commentaires(fichier_out.read()) == resulat + "\n" + signature(colloscope)
        except FileNotFoundError:
            fichier_out = open(nom_fichier_out, 'w')
            fichier_out.write(resulat + "\n" + signature(colloscope) + "\n" + commenter(colloscope))
        finally:
            fichier_out.close()


def signature(chaine_csv):
    return '{} cellule(s) sur {} ligne(s)'.format(chaine_csv.count(";"), chaine_csv.count("\n") + 1)


def ignorer_commentaires(texte):
    filtered_lines = []
    for line in texte.split("\n"):
        if line.startswith("####"):
            break
        filtered_lines.append(line)
    return "\n".join(filtered_lines)


def commenter(texte):
    return "#### Ignorer a partir d'ici\n"+ texte


def test_signature():
    assert signature("kjsld;s\nqlmk\n\njmqd;;;qsdq;da;sd\nf") \
           == "6 cellule(s) sur 5 ligne(s)"


def test_ignore_comment():
    assert ignorer_commentaires("a ne pas \nignorer \n#### Ignorer a partir d'ic\ndu texte\na igno-\nrer\n") \
           == "a ne pas \nignorer "


def test_ignore_comment_vide():
    assert ignorer_commentaires(os.linesep) == os.linesep


def test_ignore_comment_vide():
    assert ignorer_commentaires("") == ""


def test_ignore_comment_sans_contenu():
    assert ignorer_commentaires("#### Ignorer a partir d'ici\ndu texte\na igno-\nrer" + os.linesep) == ""


def test_ignore_comment_sans_commentaire():
    assert ignorer_commentaires("ne pas ignorer\ndu texte\na ne pas igno-\nrer") \
           == "ne pas ignorer\ndu texte\na ne pas igno-\nrer"


def test_commenter():
    assert commenter("texte a commenter") == "#### Ignorer a partir d'ici\ntexte a commenter"