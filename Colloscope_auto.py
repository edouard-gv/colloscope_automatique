﻿# Colloscope automatique nouvelle version
from bibli.calcul import nextgen, initforest
from bibli.edition_xml import *
from bibli.parametres import Parametres
from bibli.preparation import creation_legende, creation_matieres, creation_colleurs, creation_probas, \
    creation_ecarts, attribution_profils, verif_heures
from bibli.presentation import write_everything

def print_probleme(semaine, matiere, dispo, besoin):
    return str(abs(dispo-besoin)) + " slot" + ("s" if abs(besoin - dispo) > 1 else " ") \
           + (" manquant" + ("s" if besoin - dispo > 1 else " ") if besoin > dispo else " en trop  ") \
           + " pour "+matiere+(" sur l'annee" if semaine == None else " en sem. "+str(semaine))


def resoudre(nom_colloscope, nbgeneration, imprimer = True):
    parametres = Parametres(nom_colloscope)
    attrib, profils = attribution_profils(parametres)
    if attrib[0] == 'Bug':
        return "Probleme dans l'attribution des profils, il manque des heures" + "\n" + str(attrib[1:len(attrib)]) + str(profils), ""
        # break : pas besoin de break avant les else
    else:
        Problemes = verif_heures(attrib, profils, parametres)
        if Problemes != []:
            return "Il y a un probleme de correspondance entre les besoins et les heures dispos\n  " + \
                   "\n  ".join([print_probleme(*p) for p in Problemes]), ""
            # break
        else:
            string_retour = ""
            legende = creation_legende(parametres)
            matieres = creation_matieres(parametres, legende)
            colleurs = creation_colleurs(parametres, legende)
            probas = creation_probas(parametres, matieres, profils, attrib)
            ecarts = creation_ecarts(probas)
            Forest, Scores = initforest(parametres)
            for i in range(nbgeneration):
                forest = nextgen(Forest, Scores, parametres, legende, matieres, colleurs, probas, ecarts, profils, attrib)
                colloscope_global_imprime = write_everything(forest, parametres, legende, colleurs, imprimer)
                string_retour += 'generation ' + str(i + 1) + "\n"
                if None not in Scores[-1]:
                    string_retour += 'meilleur score :' + str(min(Scores[-1])) + "\n"
                else:
                    string_retour = 'Pas de solution trouvee\n'
            return string_retour, colloscope_global_imprime

if __name__=='__main__':
    while True:
        a=input('Que voulez-vous faire ?\n 1 : Editer le fichier xml \n 2 : Lancer la resolution \n 3: Quitter')
        if a=='1':
            b=None
            while b!='1' and b!='2':
                b=input('Le fichier est-il deja cree ?\n 1 : Oui \n 2 : Non')
                if b=='1':
                    nom_colloscope=input('Quel est le nom du colloscope deja cree ?')
                    nomfichier,nbslots,nbsemaines,nbgroupes,colonnes_legende,colonnes_colloscope,saut_de_ligne= Parametres(nom_colloscope).infos_racine()
                elif b=='2':
                    nom_colloscope=input('Entrez le nom du colloscope : ')
                    nomfichier=nom_colloscope+'.xml'
                    nbslots=input("En combien d'intervalles de temps voulez-vous decouper la journee ?")
                    nbsemaines=input("Quelle est la duree en semaines du colloscope ? ")
                    nbgroupes=input("Combien de groupes y-a-t-il ? ")
                    colonnes_legende=input('Sur combien de colonnes voulez-vous la legende ?')
                    colonnes_colloscope=input('Sur combien de colonnes voulez-vous le colloscope ?')
                    saut_de_ligne=input('Quel est le saut de ligne ? ')
                    structure(nomfichier,nbslots,nbgroupes,nbsemaines,colonnes_legende,colonnes_colloscope,saut_de_ligne)
            while True:
                c=input("Que voulez-vous faire ?\n1 : Ajouter une matiere\n2 : Ajouter un colleur\n3 : Ajouter un horaire pour un colleur\n4 : Ajouter un TP\n5: Ajouter un horaire pour un TP\n6: Supprimer une entree \n7: Revenir au menu principal")
                if c=='7':
                    break
                if c=='1':
                    matiere=input('Quel est le nom de la matiere ?')
                    d=None
                    while d!='1' and d!='2':
                        d=input('La matiere est-elle periodique ?\n1: Oui\n2: Non')
                        if d=='1':
                            periode=input('Quelle est la periode en semaines de la matiere ?')
                            debutperiode=str(int(input('Quelle est la premiere semaine ou le groupe 1 aura une colle de cette matiere ?'))-1)
                            nombredecolles='False'
                        if d=='2':
                            periode='False'
                            debutperiode='False'
                            nombredecolles=input('Quel est le nombre total de colles par groupe pour cette matiere ?')
                    sigle=input('Quel est le sigle de cette matiere pour la legende ?')
                    legende=input('Quel est le nom de cette matiere pour la legende (Laissez vide si identique au nom de la matiere) ?')
                    if legende=='':
                        legende=matiere
                    ajouter_matiere(nomfichier,matiere,periode,sigle,nombredecolles,debutperiode,legende)
                if c=='2':
                    colleur=input('Quel est le nom du colleur ?')
                    matiere=input('Quelle est la matiere du colleur ?')
                    poids=input('Quel est le poids du colleur dans le score ? (Entrez un entier, 1 par defaut)')
                    ajouter_professeur(nomfichier,colleur,matiere,poids)
                if c=='3':
                    colleur=input('A quel colleur faut-il ajouter un horaire ?')
                    matiere=input('Quelle est la matiere du colleur ?')
                    jour=input('Quel est le jour ?')
                    debut=input("Quel est l'horaire de depart (a donner en nombre d'intervalles selon le decoupage choisi au debut du fichier) ?")
                    longueur=input("Quelle est la duree (la encore en intervalles) ?")
                    e=None
                    while e not in ['1','2','3','4']:
                        e=input("Comment souhaitez-vous saisir les semaines disponibles pour cet horaire ?\n1 : En donnant une periode\n2 : Toutes les semaines sauf...\n3 : Uniquement les semaines...\n4 : Saisie manuelle")
                        if e=='1':
                            p=input('Saisissez la periode (chaine de 0 et de 1)')
                            semaines=p*(nbsemaines//len(p))+p[0:nbsemaines%len(p)]
                            ajouter_horaire(nomfichier,colleur,matiere,semaines,debut,longueur,jour)
                        if e=='2':
                            ex=input('Saisissez les semaines manquantes (separez les numeros de semaine par un espace)')
                            liste=[]
                            temp=''
                            for char in ex:
                                if char==' ' and temp!='':
                                    liste.append(temp)
                                    temp=''
                                elif char!=' ':
                                    temp+=char
                            if temp!='':
                                liste.append(temp)
                            liste=[int(s)-1 for s in liste]
                            semaines=''
                            for i in range(nbsemaines):
                                if i in liste:
                                    semaines+='0'
                                else:
                                    semaines+='1'
                            ajouter_horaire(nomfichier,colleur,matiere,semaines,debut,longueur,jour)
                        if e=='3':
                            ex=input("Saisissez les semaines pour lesquelles l'horaire est valide (separez les numeros de semaine par un espace)")
                            liste=[]
                            temp=''
                            for char in ex:
                                if char==' ' and temp!='':
                                    liste.append(temp)
                                    temp=''
                                elif char!=' ':
                                    temp+=char
                            if temp!='':
                                liste.append(temp)
                            liste=[int(s)-1 for s in liste]
                            semaines=''
                            for i in range(nbsemaines):
                                if i in liste:
                                    semaines+='1'
                                else:
                                    semaines+='0'
                            ajouter_horaire(nomfichier,colleur,matiere,semaines,debut,longueur,jour)
                        if e=='4':
                            semaines=input('Entrez la chaine de 0 et de 1 complete :')
                            ajouter_horaire(nomfichier,colleur,matiere,semaines,debut,longueur,jour)
                if c=='4':
                    intitule=input('Quel est le nom du TP ?')
                    periode=input('Quelle est sa periode en semaines ?')
                    debutperiode=str(int(input('Quelle est la premiere semaine ou le groupe 1 aura ce TP ?'))-1)
                    sigle=input('Quel est le sigle de ce TP pour la legende ?')
                    ajouter_TP(nomfichier,intitule,periode,sigle,debutperiode)
                if c=='5':
                    TP=input('A quel TP faut-il ajouter un horaire ?')
                    jour=input('Quel est le jour ?')
                    debut=input("Quel est l'horaire de depart (a donner en nombre d'intervalles selon le decoupage choisi au debut du fichier) ?")
                    longueur=input("Quelle est la duree (la encore en intervalles) ?")
                    places=input('Combien y-a-t-il de places (en nombre de groupes) pour cet horaire ?')
                    e=None
                    while e not in ['1','2','3','4']:
                        e=input("Comment souhaitez-vous saisir les semaines disponibles pour cet horaire ?\n1 : En donnant une periode\n2 : Toutes les semaines sauf...\n3 : Uniquement les semaines...\n4 : Saisie manuelle")
                        if e=='1':
                            p=input('Saisissez la periode (chaine de 0 et de 1)')
                            semaines=p*(nbsemaines//len(p))+p[0:nbsemaines%len(p)]
                            ajouter_horaire_TP(nomfichier,TP,debut,longueur,jour,places,semaines)
                        if e=='2':
                            ex=input('Saisissez les semaines manquantes (separez les numeros de semaine par un espace)')
                            liste=[]
                            temp=''
                            for char in ex:
                                if char==' ' and temp!='':
                                    liste.append(temp)
                                    temp=''
                                elif char!=' ':
                                    temp+=char
                            if temp!='':
                                liste.append(temp)
                            liste=[int(s)-1 for s in liste]
                            semaines=''
                            for i in range(nbsemaines):
                                if i in liste:
                                    semaines+='0'
                                else:
                                    semaines+='1'
                            ajouter_horaire_TP(nomfichier,TP,debut,longueur,jour,places,semaines)
                        if e=='3':
                            ex=input("Saisissez les semaines pour lesquelles l'horaire est valide (separez les numeros de semaine par un espace)")
                            liste=[]
                            temp=''
                            for char in ex:
                                if char==' ' and temp!='':
                                    liste.append(temp)
                                    temp=''
                                elif char!=' ':
                                    temp+=char
                            if temp!='':
                                liste.append(temp)
                            liste=[int(s)-1 for s in liste]
                            semaines=''
                            for i in range(nbsemaines):
                                if i in liste:
                                    semaines+='1'
                                else:
                                    semaines+='0'
                            ajouter_horaire_TP(nomfichier,TP,debut,longueur,jour,places,semaines)
                        if e=='4':
                            semaines=input('Entrez la chaine de 0 et de 1 complete :')
                            ajouter_horaire_TP(nomfichier,TP,debut,longueur,jour,places,semaines)
                if c=='6':
                    f=None
                    while f not in['1','2','3','4','5']:
                        f=input('Que voulez-vous supprimer ?\n1 : Une matiere\n2 : Un colleur\n3 : Un horaire de colleur\n4 : Un TP\n5 : Un horaire de TP')
                        if f=='1':
                            matiere=input('Quelle matiere voulez-vous supprimer ?')
                            supprimer_matiere(nomfichier,matiere)
                        if f=='2':
                            colleur=input('Quel colleur voulez-vous supprimer ?')
                            supprimer_colleur(nomfichier,colleur)
                        if f=='3':
                            colleur=input('Pour quel colleur voulez-vous supprimer un horaire ?')
                            matiere=input('Quelle est la matiere ?')
                            jour=input("Quel est le jour de l'horaire ?")
                            debut=input("Quel est le debut (en nombre d'intervalles) de l'horaire ?")
                            supprimer_horaire_colleur(nomfichier,colleur,matiere,jour,debut)
                        if f=='4':
                            TP=input('Quel TP voulez-vous supprimer ?')
                            supprimer_TP(nomfichier,TP)
                        if f=='5':
                            TP=input("Pour quel TP voulez-vous supprimer un horaire ?")
                            jour=input("Quel est le jour de l'horaire ?")
                            debut=input("Quel est le debut (en nombre d'intervalles) de l'horaire ?")
                            supprimer_horaire_TP(nomfichier,TP,jour,debut)

        elif a=='2':
            nom_colloscope=input('Quel est le nom du colloscope que vous souhaitez resoudre ?')
            nbgeneration=int(input('Sur combien de generations voulez-vous faire tourner le programme (nombre entier) ?'))
            reponse, colloscope_global = resoudre(nom_colloscope, nbgeneration)
            print(reponse)
            with open(nom_colloscope + '_colloscope.csv', 'w') as file:
                file.write(colloscope_global)
        elif a=='3':
            break
        else:
            print('Entrez 1, 2 ou 3')












